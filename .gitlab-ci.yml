stages:
  - build
  - test
  - deploy

services:
  - docker:19.03.12-dind

variables:
  DOCKER_HOST: tcp://docker:2375
  # Helm
  RELEASE_NAME: hello-world-nodejs
  CHART_ROOT: ./k8s
  # Ingress
  INGRESS_DOMAIN: apps.iammrb.com
  INGRESS_PRODUCTION: hello.iammrb.com
  # k8s
  PULL_SECRET: gitlab-registry-test

build:
  image: docker:19.03.12
  stage: build
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE || true
    - docker build --cache-from $CI_REGISTRY_IMAGE --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest

test:
  image: node:14.9.0-alpine3.10
  stage: test
  script:
    - node -v

### reset deployment ###
stop_review:
  stage: deploy
  image:
    name: alpine/helm
    entrypoint: [ "" ]
  script:
    - helm delete "$(echo $RELEASE | cut -c 1-53)" --namespace $KUBE_NAMESPACE || echo "Skipping helm release already deleted or not created"
  variables:
    GIT_STRATEGY: none
    RELEASE: ${RELEASE_NAME}-${CI_COMMIT_REF_SLUG}
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop


### template ###
.deploy_common: &deploy
  stage: deploy
  retry: 2
  image:
    name: alpine/helm
    entrypoint: [ "" ]
  script:
    - |
      CHART=${CHART_ROOT:-./k8s}
      if [ -z "$RELEASE" ]; then
        echo "[ERROR] Variable Release name not set"
        exit 1
      elif [ "${#RELEASE}" -gt 53 ]; then
        echo "[WARN] Release name exceeds maximum length, truncating to 53 characters"
        RELEASE="$(echo $RELEASE | cut -c 1-53)"
      fi
      echo "release=\"${RELEASE}\""
      if [ "${#INGRESS_HOST}" -gt 63 ]; then
        echo "[ERROR] \$INGRESS_HOST exceeds DNS RFC1035 max length of 63 characters: $INGRESS_HOST"
        echo "[ERROR] If this is a review-branch you may need to rename your branch with a shorter name"
        exit 1
      fi
      helm upgrade -i $RELEASE $CHART --namespace=$KUBE_NAMESPACE \
        --set image.repository=$CI_REGISTRY_IMAGE \
        --set image.tag=$CI_COMMIT_SHA \
        --set ingress.hosts[0].host=$INGRESS_HOST,ingress.hosts[0].paths={/} \
        --set imagePullSecrets[0].name=$PULL_SECRET \
        --set containerPort=3000

### review app ###
deploy_review:
  <<: *deploy
  variables:
    GIT_STRATEGY: none
    INGRESS_HOST: ${CI_COMMIT_REF_SLUG}.${INGRESS_DOMAIN}
    RELEASE: ${RELEASE_NAME}-${CI_COMMIT_REF_SLUG}
    DEPLOY_ENV: dev
  when: manual
  only:
    refs:
      - branches
    kubernetes: active
  except:
    refs:
      - master
    variables:
      - $DISABLE_REVIEW_DEPLOY
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://${CI_COMMIT_REF_SLUG}.${INGRESS_DOMAIN}
    on_stop: stop_review

### staging ###
deploy_staging:
  <<: *deploy
  variables:
    INGRESS_HOST: staging.${INGRESS_DOMAIN}
    RELEASE: ${RELEASE_NAME}-staging
    DEPLOY_ENV: staging

  only:
    refs:
      - master
    kubernetes: active
  except:
    variables:
      - $DISABLE_STAGING_DEPLOY

  environment:
    name: staging
    url: https://staging.${INGRESS_DOMAIN}

### production ###
deploy_prod:
  <<: *deploy

  variables:
    GIT_STRATEGY: none
    INGRESS_HOST: ${INGRESS_PRODUCTION}
    RELEASE: ${RELEASE_NAME}-prod
    DEPLOY_ENV: prod

  only:
    refs:
      - tags
    kubernetes: active
  environment:
    name: production
    url: https://${INGRESS_PRODUCTION}
