FROM node:14.9.0-alpine3.10
WORKDIR /usr/src/app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY src .
CMD ["node", "index.js"]
