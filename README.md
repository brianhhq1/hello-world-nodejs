# Hello World Nodejs

## Local Development
* OS 
```
npm init
npm install express
npm install
node src/index.js
```
### Dockerization
```
docker build -t hello-world-nodejs .
docker run -d --rm --name hello-world-nodejs -p 3000:3000 hello-world-nodejs
```

* visit http://localhost:3000

## Deployment
### k8s
```
export KUBE_NAMESPACE=sws-infra
export RELEASE=hello-world-nodejs
export INGRESS_HOST=hello.apps.simplywall.st
export CI_REGISTRY_IMAGE=registry.gitlab.com/brianhhq1/hello-world-nodejs
export CI_COMMIT_SHA=latest
export CHART=./k8s
export PULL_SECRET=gitlab-registry-test
/usr/local/Cellar/helm/3.2.4/bin/helm upgrade -i $RELEASE $CHART --namespace=$KUBE_NAMESPACE \
	--set image.repository=$CI_REGISTRY_IMAGE \
    --set image.tag=$CI_COMMIT_SHA \
    --set ingress.hosts[0].host=$INGRESS_HOST,ingress.hosts[0].paths={/} \
    --set imagePullSecrets[0].name=$PULL_SECRET \
    --set containerPort=3000

/usr/local/Cellar/helm/3.2.4/bin/helm delete $RELEASE --namespace=$KUBE_NAMESPACE
```

## Pipeline

## Highlight of the project
* [Dockerization] Docker is chosen as an artifact as it increases CI Efficiency, simplifying configurations and ensures rapid deployment. It is also good to ship products across platforms easily.
* [Performance] The principle of minimum packages/images has been considered to ensure shipping the smallest but functional application over a low-bandwidth connection network.
* [CI] Continuous Integration has been applied in this repo. GitlabCI is chosen to be the CI tool to make sure building stuff faster, never shipping broken code and decreasing code review time.
* [Process Improvement] Build caching mechanism has been considered to reduce the amount of building time.
* [Documentation] Documentation has been provided to make it easy for a user to run this program. Gitlab integrates with Gitlab badges to quickly check the status of build status.
* [Security] Branch protection rule(Signning commits with GPG) has been applied. Commits pushed to matching branches must have verified signatures to ensure no one can pretend someone to commit. This is very important to large organisations with a lot of engineers.
* [Review Branch Release] Review branch release is a very cool feature which can help engineers easily to release their features and get a feature branch url. It is also cool for stakeholders to check the features and provide feedback before merging into master.

